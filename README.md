<h1>Hi 👋, I'm Mohammad</h1>

<h4 >As a junior backend developer with two years of experience, I specialize in web development using Python and JavaScript. My expertise lies in utilizing the Django and Django Rest Framework to create dynamic and efficient web applications. Additionally, I possess hands-on experience with various JavaScript frameworks, such as React, to design engaging user interfaces.<br><br>

Furthermore, I am a fast learner and always eager to expand my skill set. Currently, I am actively learning FastAPI to further enhance my proficiency in developing high-performance, RESTful APIs.

</h4>
<hr>

<h3 align="left">Contact me Via : </h3>
<a href="https://t.me/El_mohamad"><img src="https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white"></a>
<a href="mailto:liaghimohamad69@gmail.com"><img src="https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white" /> </a>
<a href="https://www.linkedin.com/in/mohamad-liyaghi/"><img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white"></a>

<hr>


![](https://komarev.com/ghpvc/?username=mohamad-liyaghi)

<hr>

<div>
    <div style="margin: 20px;">
    <h3>Things i've used in my projects:</h3>
        <img style="height:30px;" alt="django" src="https://img.shields.io/badge/Django-030903.svg?style=flat&logo=django&logoColor=green">
        <img style="height:30px;" alt="django-rest" src="https://img.shields.io/badge/DjangoRestFrameWork-030903.svg?style=flat&logo=django&logoColor=white">
        <img style="height:30px;" alt="fastapi" src="https://img.shields.io/badge/FastAPI-030903.svg?style=flat&logo=fastapi&logoColor=green">
    <br><h3>Databases:</h3>
        <img style="height:30px;" alt="postgres" src="https://img.shields.io/badge/PostgreSQL-030903.svg?style=flat&logo=postgresql&logoColor=blue">
        <img style="height:30px;" alt="redis" src="https://img.shields.io/badge/Redis-030903.svg?style=plasic&logo=redis&logoColor=red">
        <img style="height:30px;" alt="mongodb" src="https://img.shields.io/badge/mongodb-030903.svg?style=plasic&logo=mongodb&logoColor=green">
       <h4>Testing:</h4>
        <img style="height:30px;" alt="unittest" src="https://img.shields.io/badge/UnitTest-030903.svg?style=plasic&logo=unittest&logoColor=aqua">
        <img style="height:30px;" alt="pytest" src="https://img.shields.io/badge/pytest-030903.svg?style=plasic&logo=pytest&logoColor=aqua">
    <h3>Other:</h3>
        <img style="height:30px;" alt="docker" src="https://img.shields.io/badge/Docker-030903.svg?style=plasic&logo=docker&logoColor=blue">
        <img style="height:30px;" alt="nginx" src="https://img.shields.io/badge/nginx-030903.svg?style=flat&logo=nginx&logoColor=green">
        <img style="height:30px;" alt="git" src="https://img.shields.io/badge/Git-030903.svg?style=plasic&logo=git&logoColor=orange">
        <img style="height:30px;" alt="elastic" src="https://img.shields.io/badge/elastic-030903.svg?style=plasic&logo=elastic">
        <img style="height:30px;" alt="rabbitmq" src="https://img.shields.io/badge/rabbitmq-030903.svg?style=flat&logo=rabbitmq&logoColor=orange">
    </div>
</div>

<hr>
<h3>Donation: </h3>

[![Donate](https://img.shields.io/badge/Donate-FFDD00?style=for-the-badge&logo=buy-me-a-coffee&logoColor=black)](https://www.coffeete.ir/mohamad_liyaghi)

In case you are not in Iran: <br>

[![Donate](https://img.shields.io/badge/Donate-FFDD00?style=for-the-badge&logo=buy-me-a-coffee&logoColor=black)](https://www.buymeacoffee.com/ml06py)


<details>
  <summary><h3>Github Stats</h3></summary>
  
  <a href="#">![Github stats](https://github-readme-stats.vercel.app/api?username=mohamad-liyaghi&theme=blueberry&count_private=true&hide_border=true&line_height=20)</a>
  <a href="#">![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=mohamad-liyaghi&layout=compact&theme=blueberry&count_private=true&hide_border=true)</a>

  ![](https://github-profile-summary-cards.vercel.app/api/cards/profile-details?username=mohamad-liyaghi&theme=blueberry)

</details>





